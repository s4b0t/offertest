<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController
{
    public function index() {

        $results = DB::table('products')
            ->selectRaw(
                'count(offers.id) as total,
                avg(offers.price) as srednee,
                products.id,
                products.name'
            )
            ->leftJoin('offer_product', 'products.id','offer_product.product_id')
            ->leftJoin('offers', 'offer_product.offer_id','offers.id')
            ->groupBy([
                'products.id',
                'products.name'
            ])
            ->get()->toArray();

        dd($results);
    }
}
